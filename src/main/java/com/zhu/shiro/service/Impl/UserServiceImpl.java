package com.zhu.shiro.service.Impl;

import com.zhu.shiro.dao.UserDao;
import com.zhu.shiro.entity.User;
import com.zhu.shiro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
