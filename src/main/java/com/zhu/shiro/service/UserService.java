package com.zhu.shiro.service;

import com.zhu.shiro.entity.User;

public interface UserService {

    User findByUsername(String username);
}
