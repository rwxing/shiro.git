package com.zhu.shiro.dao;

import com.zhu.shiro.entity.User;

public interface UserDao {
    User findByUsername(String username);
}
