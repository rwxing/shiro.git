package com.zhu.shiro.service.Impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.zhu.shiro.entity.Permission;
import com.zhu.shiro.entity.Role;
import com.zhu.shiro.entity.User;
import com.zhu.shiro.service.UserService;
import javafx.css.CssParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
    @Autowired
    private UserService userService;
    @Test
    public void findByUsername() {
        User u = userService.findByUsername("tom");
        Set<Role> roleSet = u.getRoles();
        for (Role role : roleSet){
            Set<Permission> permissionSet = role.getPermissions();
            for (Permission permission : permissionSet){
                System.out.println(permission.getName());
            }
            System.out.println(role.getName());
        }
    }
}